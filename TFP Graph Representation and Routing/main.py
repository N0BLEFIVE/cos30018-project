import math
import heapq
import sys
import csv

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from keras.models import load_model
from keras.utils.vis_utils import plot_model
import sklearn.metrics as metrics

"""
to do, move edges to be inside vertexes. X
"""


"""
Vertexes are data structure representations
of the intersection which are identifiable by
scats numbers.

Each are located geographically through longitude and 
latitude values.
"""
class Vertex:
    def __init__(self, scat_num, lat = 0.0, long = 0.0):
        self.scat_num = scat_num
        self.lat = lat
        self.long = long
        self.travel_cost = sys.maxsize
        self.edges = {}
        self.visited = False
        self.previous = None

    def __str__(self):
        return "{id}, x = {x:0.2f}, y = {y:0.2f}".format(id = self.scat_num, x= self.lat, y=self.long)

    def __repr__(self):
        return str(self)

    def set_lat(self, lat):
        self.lat = lat

    def get_lat(self):
        return self.lat

    def set_long(self, long):
        self.long = long

    def get_long(self):
        return self.long

    def get_id(self):
        return self.scat_num
    
    def __eq__(self, scat_num):
        return self.scat_num == scat_num

    def __hash__(self):
        return hash(str(self.scat_num))

    def set_current_cost(self, cost):
        self.travel_cost = cost

    def get_current_cost(self):
        return self.travel_cost

    def add_edge(self, edge):
        self.edges[edge.end.scat_num] = edge
    
    def get_edge(self, edge):
        return self.edges[edge]

    def remove_edge(self, edge):
        self.edges.pop(edge)

    def set_previous(self, prev):
        self.previous = prev

    def set_visited(self):
        self.visited = True

    def get_travel_time(self, adjacent_site):
        return self.edges[adjacent_site].get_travel_time()

    def __lt__(self, other):
        return self.travel_cost < other.travel_cost


"""
Edges object representations of the street between scat sites.
Each street or road has a detector that is responsible for tracking traffic within
the street. Within edges there are detector identifiers, starting point, endpoint, and traffic.
variables such as distance, density, trvael speed, and travel time are calculated within the object.
"""
class Edge:
    def __init__(self, detector, start, end,  flow = 0.0):
        self.detector = detector
        self.start = start
        self.end = end
        self.flow = flow
        self.distance = self.calculate_distance(start, end)
        self.density = 0
        self.travel_speed = 0
        self.travel_time = 30

        if (self.distance != 0):
            self.density = self.flow/self.distance
            self.travel_speed = (self.flow/self.density) * 4
            self.travel_time = self.distance/self.travel_speed


    def __str__(self):
        return "({start}, {end}, {distance})".format(start = self.start, end = self.end, distance = self.distance)

    def __repr__(self):
        return str(self)

    def calculate_distance(self, start, end):
        return math.sqrt( ((end.lat-start.lat)**2)+((end.long-start.long)**2) ) * 111

    def set_traffic(self, flow):
        self.flow = flow

    def get_traffic(self):
        return self.flow

    def set_distance(self, distance):
        self.distance = distance
        
    def get_distance(self):
        return self.distance

    def set_start_coordinates(self, lat,long):
        self.start.set_lat(lat)
        self.start.set_log(long)

    def set_end_coordinates(self, lat,long):
        self.end.set_lat(lat)
        self.end.set_log(long)
    
    def recalculate_distance(self):
        self.distance = self.calculate_distance(self.start, self.end)

    def recalculate_density(self):
        self.density = (self.flow/15)/self.distance 

    def recalculate_travel(self):
        self.recalculate_density()
        if self.density == 0:
            self.travel_time = 30
        else:
            self.travel_speed = (self.flow/self.density)/(15*60)
            if self.travel_speed > 60/(60*60):
                self.travel_speed = 60/(60*60)
            self.travel_time = (self.distance/self.travel_speed) + 30 

    def get_travel_time(self):
        return self.travel_time

    def __lt__(self, other):
        return self.travel_time < other.travel_time
  

"""
Graph object is the general representation of the entire road network.
Within the the graph there are multiple edges. The same data structure is used to compute for the shortest path.
"""
class Graph:
    def __init__(self, edges=[]):
 
        self.adjacency_list = {}
        self.num_vertices=0

    def load_coordinates(self):
        with open('TFP Graph Representation and Routing\data\scat_coords.csv', mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                if row['SCATS Number'] in self.adjacency_list:
                    site = self.get_vertex(row['SCATS Number'])
                    site.set_lat(float(row['NB_LATITUDE']))
                    site.set_long(float(row['NB_LONGITUDE']))
                    # print(site)
            self.recaulculate_edge_distances()
            
    def recaulculate_edge_distances(self):
        for node in self.adjacency_list:
            for edge in self.adjacency_list[node].edges.values():
                edge.recalculate_distance()

    def add_node(self, node):
        self.num_vertices = self.num_vertices + 1
        new_vertex = Vertex(node)
        self.adjacency_list[node] = new_vertex
        return new_vertex

    def add_edge(self,detector,scat_start, scat_end):
        if scat_start not in self.adjacency_list:
            self.add_node(scat_start)
        if scat_end not in self.adjacency_list:
            self.add_node(scat_end)

        edge = Edge(detector,self.get_vertex(scat_start),self.get_vertex(scat_end))
        self.adjacency_list[scat_start].add_edge(edge)

    def add_complete_edge(self,scat_start, edge):
        self.adjacency_list[scat_start].add_edge(edge)
            
    def loadPredictions(self, prediction_df):
        for node in self.adjacency_list:
            for edge in self.adjacency_list[node].edges.values():
                if edge.detector in prediction_df:
                    edge.set_traffic(float(prediction_df[edge.detector]))
        self.recaulculate_travel_time()

    def recaulculate_travel_time(self):
        for node in self.adjacency_list:
            for edge in self.adjacency_list[node].edges.values():
                edge.recalculate_travel()

    def print_predictions(self):
        print("predictions")
        for node in self.adjacency_list:
            for edge in self.adjacency_list[node].edges.values():
                print(f"[{node}]", end='')
                print(f"(detector \"{edge.detector}\", end:{edge.end.scat_num}, traffic flow: {edge.flow:.2f} veh/15mins, travel distance: {edge.distance:.2f} km,travel speed: {(edge.travel_speed*60*60):.2f} km/hr, travel time: {edge.travel_time:.2f} secs) \n", end='')
            print()
    
    def printAdjacencyList(self):
        print(self.adjacency_list)
  
    def printGraph(self):
        print("[start](end,detector,distance) ...")
        for node in self.adjacency_list:
            print(f"[{node}]", end='')
            for edge in self.adjacency_list[node].edges.values():
                print(f"(detector \"{edge.detector}\", end:{edge.end}, distance {edge.distance:0.2f} km) ", end='')
            print()
    
    def get_vertex(self, scat_num):
        if scat_num in self.adjacency_list:
            return self.adjacency_list[scat_num]
        else:
            return None

    def get_edges(self, scat_num):
        return self.adjacency_list[scat_num].edges

    def get_edge(self, start, end):
        return self.adjacency_list[start].get_edge(end)

    def remove_edge(self, start, end):
        for n in self.adjacency_list[start].edges:
            print (n)
        print("after ")
        self.adjacency_list[start].remove_edge(end)
        for n in self.adjacency_list[start].edges:
            print (n)

    def flush(self):
        for node in self.adjacency_list:
            self.adjacency_list[node].set_current_cost(sys.maxsize)
            self.adjacency_list[node].visited = False

    def dijkstra(self, start, target):
        print('''Dijkstra's shortest path''')
        # Set the distance for the start node to zero 
        start.set_current_cost(0)

        # Put tuple pair into the priority queue
        unvisited_queue = [(v.get_current_cost(), v) for v in self.adjacency_list.values()]
        heapq.heapify(unvisited_queue)

        while len(unvisited_queue):
            # Pops a vertex with the smallest distance 
            uv = heapq.heappop(unvisited_queue)
            current = uv[1]
            current.visited = True

            # for next in v.adjacent:
            for next in current.edges.values():
                # if visited, skip
                if next.end.visited:
                    continue

                new_cost = current.get_current_cost() + current.get_travel_time(next.end.scat_num)

                if new_cost < next.end.get_current_cost():
                    print("updated : current = {current} next = {next} new cost = {distance} vs current cost = {prev}".format(current = current.get_id(), next = next.end.get_id(), distance= new_cost, prev = next.end.get_current_cost()))
                    next.end.set_current_cost(new_cost)
                    next.end.set_previous(current)
                    
                else:
                    print("not updated : current = {current} next = {next} new cost = {distance} vs current cost = {prev}".format(current = current.get_id(), next = next.end.get_id(), distance= new_cost, prev = next.end.get_current_cost()))

            # Rebuild heap
            # 1. Pop every item
            while len(unvisited_queue):
                heapq.heappop(unvisited_queue)
            # 2. Put all vertices not visited into the queue
            unvisited_queue = [(v.get_current_cost(), v) for v in self.adjacency_list.values() if not v.visited ]
            heapq.heapify(unvisited_queue)

        
        path = [target.get_id()]
        self.shortest(target, path)
        
        return (path[::-1],target.get_current_cost())

    def shortest(self, v, path):
        ''' make shortest path from v.previous'''
        if v.previous:
            path.append(v.previous.get_id())
            self.shortest(v.previous, path)
        return

    def shortest_paths(self, start, end,model, start_date):
        predictor = TrafficPrediction()
        predictions = predictor.predict(start_date,model)
        self.loadPredictions(predictions)

        print(start,end, start_date)
        print(self.get_vertex(start))

        paths = []
        shortest_path = self.dijkstra(self.get_vertex(start), self.get_vertex(end))
        paths.append(shortest_path)
        for i in range(1,len(shortest_path[0])):
            print(shortest_path[0][i-1], shortest_path[0][i])
            self.flush()
            re = self.get_edge(shortest_path[0][i-1], shortest_path[0][i])
            self.remove_edge(re.start.get_id(),re.end.get_id())
            paths.append(self.dijkstra(self.get_vertex(start), self.get_vertex(end)))
            self.add_complete_edge(re.start.get_id(), re)

        paths.sort(key = lambda x: x[1]) 

        return paths


class TrafficPrediction:
    def __init__(self):
        self.lstm = load_model('models\LSTM.h5')
        self.gru = load_model('models\GRU.h5')
        self.cnn = load_model('models\CNN.h5')
        self.lags = 12

    def process_input(self, start_datetime, end = 0.0):
        train_df = pd.read_csv('./TFP Graph Representation and Routing/data/train.csv', encoding='utf-8').fillna(0)
        test_df = pd.read_csv('./TFP Graph Representation and Routing/data/test.csv', encoding='utf-8').fillna(0)

        column_names = test_df.columns.tolist()

        datetime_label = pd.DataFrame(test_df["Date"], columns = ["Date"])
        o = datetime_label.index[datetime_label['Date'] == start_datetime]

        test_df = test_df[o[0]-self.lags:o[0]+1]
        
        scaler = MinMaxScaler()
        scaler.fit(train_df.loc[:, train_df.columns != 'Date'])
        
        scaled_test_df = test_df
        scaled_test_df.loc[:, test_df.columns != 'Date'] = scaler.transform(test_df.loc[:, test_df.columns != 'Date'])
        
        scaled_test_df_values = scaled_test_df.loc[:, scaled_test_df.columns != 'Date'].values

        test = []

        for i in range(self.lags, len(scaled_test_df_values)):
            test.append(scaled_test_df_values[i - self.lags: i + 1])

        test = np.array(test)

        X_test = test[:, :-1]
        y_test = test[:, -1]

        return X_test, y_test, scaler, column_names

    def predict(self, start_datetime, model_to_use = "lstm"):
        if model_to_use == "lstm": 
            model = self.lstm
        if model_to_use == "gru": 
            model = self.gru
        if model_to_use == "cnn": 
            model = self.cnn

        X_test, y_test, scaler, column_names = self.process_input(start_datetime)

        predicted = model.predict(X_test)
        predicted = scaler.inverse_transform(predicted)

        prediction_df = pd.DataFrame(predicted,columns=column_names[1:])
        return prediction_df

 
if __name__ == '__main__':
  
    graph = Graph()

    graph.add_edge("WARRIGAL_RD N of HIGH STREET_RD",'0970', '3685')
    graph.add_edge("WARRIGAL_RD W of HIGH STREET_RD",'0970', '2846')
    graph.add_edge("WARRIGAL_RD N of TOORAK_RD",'2000', '3682')
    graph.add_edge("WARRIGAL_RD S of BURWOOD_HWY",'2000', '3685')
    graph.add_edge("TOORAK_RD W of WARRIGAL_RD",'2000', '4043')
    graph.add_edge("MAROONDAH_HWY W of UNION_RD",'2200', '4063')
    graph.add_edge("UNION_RD S of MAROONDAH_HWY",'2200', '3126')
    graph.add_edge("PRINCESS_ST S of CHANDLER_HWY",'2820', '3662')
    graph.add_edge("BURKE_RD S of EASTERN_FWY",'2825', '4030')
    graph.add_edge("BULLEEN_RD S of EASTERN_FWY_W_BD_RAMPS",'2827', '4051')
    graph.add_edge("HIGH_ST E OF S.E.ARTERIAL",'2846', '0970')
    graph.add_edge("WILLS_ST NW OF HIGH_ST",'2846', '4043')
    graph.add_edge("CHURCH_ST SW of BARKERS_RD",'3001', '4262')
    graph.add_edge("HIGH_ST NE of BARKERS_RD",'3001', '3662')
    graph.add_edge("BARKERS_RD E of HIGH_ST",'3001', '3002')
    graph.add_edge("BARKERS_RD W of CHURCH_ST",'3001', '4821')
    graph.add_edge("BARKERS_RD W of DENMARK_ST",'3002', '3001')
    graph.add_edge("BARKERS_RD E of DENMARK_ST",'3002', '4035')
    graph.add_edge("DENMARK_ST N of BARKERS_RD",'3002', '3662')
    graph.add_edge("POWER_ST S of BARKERS_RD",'3002', '4263')
    graph.add_edge("BURKE_RD N of CANTERBURY_RD",'3120', '4035')
    graph.add_edge("CANTERBURY_RD E of BURKE_RD",'3120', '3122')
    graph.add_edge("BURKE_RD S of CANTERBURY_RD",'3120', '4040')
    graph.add_edge("CANTERBURY_RD W of STANHOPE_GV",'3122', '3120')
    graph.add_edge("CANTERBURY_RD E of STANHOPE_GV",'3122', '3127')
    graph.add_edge("STANHOPE_GV S of CANTERBURY_RD",'3122', '3804')
    # there was no connection from 3126 to 2200 since it didnt have a north detector
    #there was no scat site adjacent to the east detector 
    graph.add_edge("WARRIGAL_RD S of CANTERBURY_RD",'3126', '3682')
    graph.add_edge("CANTERBURY_RD W of WARRIGAL_RD",'3126', '3127')
    graph.add_edge("BALWYN_RD N of CANTERBURY_RD",'3127', '4063')
    graph.add_edge("CANTERBURY_RD W of BALWYN_RD",'3127', '3122')
    graph.add_edge("CANTERBURY_RD E of BALWYN_RD",'3127', '3126')
    graph.add_edge("DONCASTER_RD W of BALWYN_RD",'3180', '4051')
    graph.add_edge("BALWYN_RD S of DONCASTER_RD",'3180', '4057')
    #there was no connection from 3662 to 3002 since it didnt have a south detector
    graph.add_edge("HIGH_ST SW of DENMARK_ST",'3662', '3001')
    graph.add_edge("PRINCESS_ST N of HIGH_ST",'3662', '2820')
    graph.add_edge("HIGH_ST NE of PRINCESS_ST",'3662', '4335')
    #there was no connection from 3662 to 4324 unless if you use the same road as above, then there will an issue will loop iteration?
    #graph.add_edge("a",'3662', '4324')
    graph.add_edge("WARRIGAL_RD N OF RIVERSDALE_RD",'3682', '3126')
    graph.add_edge("RIVERSDALE_RD W OF WARRIGAL_RD",'3682', '3804')
    graph.add_edge("WARRIGAL_RD S OF RIVERSDALE_RD",'3682', '2000')
    #no east detector labelled since there is no scat site on east of 3682
    graph.add_edge("WARRIGAL_RD N of HIGHBURY_RD",'3685', '2000')
    graph.add_edge("WARRIGAL_RD S of HIGHBURY_RD",'3685', '0970')
    #no east detector labelled since there is no scat site on east of 3685
    graph.add_edge("RIVERSDALE_RD E of TRAFALGAR_RD",'3804', '3682')
    graph.add_edge("TRAFALGAR_RD S of RIVERSDALE_RD",'3804', '3812')
    graph.add_edge("RIVERSDALE_RD W of TRAFALGAR_RD",'3804', '4040')
    graph.add_edge("TRAFALGAR_RD N of RIVERSDALE_RD",'3804', '3122')
    graph.add_edge("TRAFALGAR_RD NE of CAMBERWELL_RD",'3812', '3804')
    graph.add_edge("CAMBERWELL_RD NW of TRAFALGAR_RD",'3812', '4040')
    # west and southeast detectors not labelled cus it does appear to have connecting scat sites- inquire
    #no node for northeast conencting from 4030 to 4051 -  it is a one way
    #graph.add_edge("a",'4030', '4051')
    graph.add_edge("BURKE_RD S of DONCASTER_RD",'4030', '4032')
    #no detector for the connecting to north from 4030 to 2825
    #graph.add_edge("a",'4030', '2825')
    graph.add_edge("HIGH_ST SW of BURKE_RD",'4030', '4321')# double check this detector
    graph.add_edge("BURKE_RD N of HARP_RD",'4032', '4030')
    graph.add_edge("HARP_RD W of BURKE_RD",'4032', '4321')
    graph.add_edge("BELMORE_RD E of BURKE_RD",'4032', '4057')
    graph.add_edge("BURKE_RD S of HARP_RD",'4032', '4034')
    graph.add_edge("WHITEHORSE_RD E OF BURKE_RD",'4034', '4063')
    graph.add_edge("BURKE_RD N OF WHITEHORSE_RD",'4034', '4032')
    graph.add_edge("BURKE_RD S OF WHITEHORSE_RD",'4034', '4035')
    graph.add_edge("COTHAM_RD W OF BURKE_RD",'4034', '4324')
    graph.add_edge("BARKERS_RD W of BURKE_RD",'4035', '3002')
    graph.add_edge("BURKE_RD S of BARKERS_RD",'4035', '3120')
    graph.add_edge("BURKE_RD N of MONT ALBERT_RD",'4035', '4034')
    graph.add_edge("RIVERSDALE_RD W of BURKE_RD",'4040', '4272')
    graph.add_edge("CAMBERWELL_RD NW of BURKE_RD",'4040', '4266')
    graph.add_edge("BURKE_RD N of RIVERSDALE_RD",'4040', '3120')
    graph.add_edge("RIVERSDALE_RD E of BURKE_RD",'4040', '3804')
    graph.add_edge("CAMBERWELL_RD SE of BURKE_RD",'4040', '3812')
    graph.add_edge("BURKE_RD S of RIVERSDALE_RD",'4040', '4043')
    graph.add_edge("BURKE_RD S of TOORAK_RD",'4043', '2846')
    graph.add_edge("TOORAK_RD E of BURKE_RD",'4043', '2000')
    graph.add_edge("TOORAK_RD W OF BURKE_RD",'4043', '4273')
    graph.add_edge("BURKE_RD N of TOORAK_RD",'4043', '4040')
    graph.add_edge("SEVERN_ST S of DONCASTER_RD",'4051', '2827')
    graph.add_edge("DONCASTER_RD SW of SEVERN_ST",'4051', '4030')
    graph.add_edge("DONCASTER_RD E of BULLEEN_RD",'4051', '3180')
    graph.add_edge("BALWYN_RD N OF BELMORE_RD",'4057', '3180')
    graph.add_edge("BALWYN_RD S OF BELMORE_RD",'4057', '4063')
    graph.add_edge("BELMORE_RD W OF BALWYN_RD",'4057', '4032')
    graph.add_edge("BALWYN_RD N OF WHITEHORSE_RD",'4063', '4057')
    graph.add_edge("WHITEHORSE_RD E OF BALWYN_RD",'4063', '2200')
    graph.add_edge("BALWYN_RD S OF WHITEHORSE_RD",'4063', '3127')
    graph.add_edge("WHITEHORSE_RD W OF BALWYN_RD",'4063', '4034')

    graph.add_edge("BRIDGE_RD SW of BURWOOD_RD",'4262', '4263') #not sure
    graph.add_edge("BRIDGE_RD SW of BURWOOD_RD",'4262', '3001') #Ithink both will have the same detector since theres only one
    graph.add_edge("BURWOOD_RD E of POWER_ST",'4263', '4264')
    graph.add_edge("BURWOOD_RD W of POWER_ST",'4263', '4262')
    graph.add_edge("POWER_ST N of BURWOOD_RD",'4263', '3002')
    graph.add_edge("GLENFERRIE_RD S of BURWOOD_RD",'4264', '4270')
    graph.add_edge("BURWOOD_RD E of GLENFERRIE_RD",'4264', '4266')
    graph.add_edge("BURWOOD_RD W of GLENFERRIE_RD",'4264', '4263')
    graph.add_edge("GLENFERRIE_RD N of BURWOOD_RD",'4264', '4324')
    #north and south unimportant for connecting from 4266 
    graph.add_edge("BURWOOD_RD W of AUBURN_RD",'4266', '4264')
    graph.add_edge("BURWOOD_RD E of AUBURN_RD",'4266', '4040')
    graph.add_edge("RIVERSDALE_RD E of GLENFERRIE_RD",'4270', '4272')
    graph.add_edge("GLENFERRIE_RD N of RIVERSDALE_RD",'4270', '4264')
    graph.add_edge("RIVERSDALE_RD W of GLENFERRIE_RD",'4270', '4812')
    #north unimportant for connectiong from 4272
    graph.add_edge("TOORONGA_RD S of RIVERSDALE_RD",'4272', '4273')
    graph.add_edge("RIVERSDALE_RD E of TOORONGA_RD",'4272', '4040')
    graph.add_edge("RIVERSDALE_RD W of TOORONGA_RD",'4272', '4270')
    graph.add_edge("TOORAK_RD E of TOORONGA_RD",'4273', '4043')
    graph.add_edge("TOORONGA_RD N of TOORAK_RD",'4273', '4272')
    graph.add_edge("HIGH_ST NE OF HARP_ST",'4321', '4030')
    graph.add_edge("HIGH_ST SW OF HARP_ST",'4321', '4335')
    #added the next two connectors below
    graph.add_edge("VALERIE_ST W OF HIGH_ST",'4321', '2820')
    graph.add_edge("HARP_RD E OF HIGH_ST",'4321', '4032')
    graph.add_edge("COTHAM_RD W OF GLENFERRIE_RD",'4324', '3662')
    graph.add_edge("COTHAM_RD E OF GLENFERRIE_RD",'4324', '4034')
    graph.add_edge("GLENFERRIE_RD S OF COTHAM_RD",'4324', '4264')
    #theres a duplication in the scat site location ,One might need to be changed to NW instead of NE, ending 3662 would be NW i assume
    graph.add_edge("HIGH_ST NE of CHARLES_ST",'4335', '3662')
    graph.add_edge("HIGH_ST NE of CHARLES_ST",'4335', '4321')
    #SW and NE unimport for connecting 4812 - DOUBLE CHECK
    graph.add_edge("MADDEN_GV S OF SWAN_ST",'4812', '4270')
    #N,S and W unimportant for connecting 4821
    graph.add_edge("VICTORIA_ST E OF BURNLEY_ST",'4821', '3001')

    graph.load_coordinates()
    # predictor = TrafficPrediction()
    # predictions = predictor.predict("2006-10-29 09:00:00")
    # graph.loadPredictions(predictions)

    # inclose in a function (start)
    start = '0970'
    end = '4264'

    print(graph.shortest_paths(start, end,"gru", "2006-10-29 09:00:00"))

    # paths = []
    # shortest_path = graph.dijkstra(graph.get_vertex(start), graph.get_vertex(end))
    # paths.append(shortest_path)
    # for i in range(1,len(shortest_path[0])):
    #     print(shortest_path[0][i-1], shortest_path[0][i])
    #     graph.flush()
    #     re = graph.get_edge(shortest_path[0][i-1], shortest_path[0][i])
    #     graph.remove_edge(re.start.get_id(),re.end.get_id())
    #     paths.append(graph.dijkstra(graph.get_vertex(start), graph.get_vertex(end)))
    #     graph.add_complete_edge(re.start.get_id(), re)

    # print(paths)

    
    




