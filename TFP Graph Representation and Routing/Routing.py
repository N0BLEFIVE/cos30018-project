import sys
import heapq


class Vertex:
    def __init__(self, node):
        self.id = node
        self.adjacent = {}
        # Set distance to infinity for all nodes
        self.distance = sys.maxsize
        # Mark all nodes unvisited        
        self.visited = False
        # Predecessor
        self.previous = None

    def __lt__(self, other):
        return self.distance < other.distance

    def add_neighbor(self, neighbor, weight=0):
        self.adjacent[neighbor] = weight

    def get_connections(self):
        return self.adjacent.keys()

    def get_id(self):
        return self.id

    def get_weight(self, neighbor):
        return self.adjacent[neighbor]

    def set_distance(self, dist):
        self.distance = dist

    def get_distance(self):
        return self.distance

    def set_previous(self, prev):
        self.previous = prev

    def set_visited(self):
        self.visited = True

    def __str__(self):
        return str(self.id) + ' adjacent: ' + str([x.id for x in self.adjacent]) + ' distance:' + str(self.distance)

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(str(self.id))


class Graph:
    def __init__(self):
        self.vert_dict = {}
        self.num_vertices = 0

    def __iter__(self):
        return iter(self.vert_dict.values())

    def add_node(self, node):
        self.num_vertices = self.num_vertices + 1
        new_vertex = Vertex(node)
        self.vert_dict[node] = new_vertex
        return new_vertex

    def get_vertex(self, n):
        if n in self.vert_dict:
            return self.vert_dict[n]
        else:
            return None

    def add_edge(self, frm, to, cost=0):
        if frm not in self.vert_dict:
            self.add_node(frm)
        if to not in self.vert_dict:
            self.add_node(to)

        self.vert_dict[frm].add_neighbor(self.vert_dict[to], 0)
        self.vert_dict[to].add_neighbor(self.vert_dict[frm], 0)

    def get_vertices(self):
        return self.vert_dict.keys()

    def set_previous(self, current):
        self.previous = current

    def get_previous(self):
        return self.previous

    def printgraph(self):
        print('Graph data:')
        count = 0
        for v in g:
            for w in v.get_connections():
                vid = v.get_id()
                wid = w.get_id()
                count += 1
                print(f"{count} (Starting node:{vid} -> End Node:{wid}, Weight of Path:{v.get_weight(w)}) ", end='')
                print()

    def test(self):
        print([(v.get_distance(), v) for v in self.vert_dict.values()])
        print(len([(v.get_distance(), v) for v in self.vert_dict.values()]))

    def test2(self):
        unvisited_queue = [(v.get_distance(), v) for v in self.vert_dict.values()]
        heapq.heapify(unvisited_queue)
        print(heapq.heappop(unvisited_queue))

    def test3(self):
        print([(v) for v in self.vert_dict.values()])
        print(len([(v.get_distance(), v) for v in self.vert_dict.values()]))


def shortest(v, path):
    ''' make shortest path from v.previous'''
    if v.previous:
        path.append(v.previous.get_id())
        shortest(v.previous, path)
    return


def dijkstra(aGraph, start, target):
    print('''Dijkstra's shortest path''')
    # Set the distance for the start node to zero 
    start.set_distance(0)

    # Put tuple pair into the priority queue
    unvisited_queue = [(v.get_distance(), v) for v in aGraph]
    heapq.heapify(unvisited_queue)

    while len(unvisited_queue):
        # Pops a vertex with the smallest distance 
        uv = heapq.heappop(unvisited_queue)
        current = uv[1]
        current.set_visited()

        # for next in v.adjacent:
        for next in current.adjacent:
            # if visited, skip
            if next.visited:
                continue
            new_dist = current.get_distance() + current.get_weight(next)

            print(new_dist)
            print(next.get_distance())
            print(new_dist < next.get_distance())

            if new_dist < next.get_distance():
                next.set_distance(new_dist)
                next.set_previous(current)
                print("updated : current = {current} next = {next} new_dist = {distance}".format(current = current.get_id(), next = next.get_id(), distance= next.get_distance()))

            else:
                print("not updated : current = {current} next = {next} new_dist = {distance}".format(current = current.get_id(), next = next.get_id(), distance= next.get_distance()))

        print("end of for")

        # Rebuild heap
        # 1. Pop every item
        while len(unvisited_queue):
            heapq.heappop(unvisited_queue)
        # 2. Put all vertices not visited into the queue
        unvisited_queue = [(v.get_distance(), v) for v in aGraph if not v.visited]
        heapq.heapify(unvisited_queue)


if __name__ == '__main__':

    g = Graph()

    g.add_node('0970')
    g.add_node('2000')
    g.add_node('2200')
    g.add_node('2820')
    g.add_node('2825')
    g.add_node('2827')
    g.add_node('2846')
    g.add_node('3001')
    g.add_node('3002')
    g.add_node('3120')
    g.add_node('3122')
    g.add_node('3126')
    g.add_node('3127')
    g.add_node('3180')
    g.add_node('3662')
    g.add_node('3682')
    g.add_node('3685')
    g.add_node('3804')
    g.add_node('3812')
    g.add_node('4030')
    g.add_node('4032')
    g.add_node('4034')
    g.add_node('4035')
    g.add_node('4040')
    g.add_node('4043')
    g.add_node('4051')
    g.add_node('4057')
    g.add_node('4063')
    g.add_node('4262')
    g.add_node('4263')
    g.add_node('4264')
    g.add_node('4266')
    g.add_node('4270')
    g.add_node('4272')
    g.add_node('4273')
    g.add_node('4321')
    g.add_node('4324')
    g.add_node('4335')
    g.add_node('4812')
    g.add_node('4821')

    g.add_edge('0970', '3685', 12)
    g.add_edge('0970', '2846', 2)
    g.add_edge('2000', '3682', 3)
    g.add_edge('2000', '3685', 4)
    g.add_edge('2000', '4043', 5)
    g.add_edge('2200', '4063', 6)
    g.add_edge('2200', '3126', 7)
    g.add_edge('2820', '3662', 8)
    g.add_edge('2825', '4030', 9)
    g.add_edge('2827', '4051', 10)
    g.add_edge('2846', '0970', 1)
    g.add_edge('2846', '4043', 2)
    g.add_edge('3001', '4262', 3)
    g.add_edge('3001', '3662', 3)
    g.add_edge('3001', '3002', 4)
    g.add_edge('3001', '4821', 5)
    g.add_edge('3002', '3001', 6)
    g.add_edge('3002', '4035', 7)
    g.add_edge('3002', '3662', 7)
    g.add_edge('3002', '4263', 8)
    g.add_edge('3120', '4035', 9)
    g.add_edge('3120', '3122', 10)
    g.add_edge('3120', '4040', 12)
    g.add_edge('3122', '3120', 1)
    g.add_edge('3122', '3127', 2)
    g.add_edge('3122', '3804', 3)
    g.add_edge('3126', '2200', 4)
    g.add_edge('3126', '3682', 5)
    g.add_edge('3126', '3127', 6)
    g.add_edge('3127', '4063', 7)
    g.add_edge('3127', '3122', 8)
    g.add_edge('3127', '3126', 9)
    g.add_edge('3180', '4051', 10)
    g.add_edge('3180', '4057', 1)
    g.add_edge('3662', '3002', 11)
    g.add_edge('3662', '3001', 12)
    g.add_edge('3662', '2820', 1)
    g.add_edge('3662', '4335', 2)
    g.add_edge('3662', '4324', 3)
    g.add_edge('3682', '3126', 4)
    g.add_edge('3682', '3804', 5)
    g.add_edge('3682', '2000', 6)
    g.add_edge('3685', '2000', 7)
    g.add_edge('3685', '0970', 8)
    g.add_edge('3804', '3682', 9)
    g.add_edge('3804', '3812', 10)
    g.add_edge('3804', '4040', 11)
    g.add_edge('3804', '3122', 12)
    g.add_edge('3812', '3804', 13)
    g.add_edge('3812', '4040', 14)
    g.add_edge('4030', '4051', 1)
    g.add_edge('4030', '2825', 2)
    g.add_edge('4030', '4321', 3)
    g.add_edge('4032', '4030', 4)
    g.add_edge('4032', '4321', 5)
    g.add_edge('4032', '4057', 6)
    g.add_edge('4032', '4034', 7)
    g.add_edge('4034', '4063', 8)
    g.add_edge('4034', '4032', 9)
    g.add_edge('4034', '4035', 1)
    g.add_edge('4034', '4324', 2)
    g.add_edge('4035', '3002', 3)
    g.add_edge('4035', '3120', 4)
    g.add_edge('4035', '4034', 5)
    g.add_edge('4040', '4272', 6)
    g.add_edge('4040', '4266', 7)
    g.add_edge('4040', '3120', 8)
    g.add_edge('4040', '3804', 9)
    g.add_edge('4040', '3812', 1)
    g.add_edge('4040', '4043', 2)
    g.add_edge('4043', '2846', 3)
    g.add_edge('4043', '2000', 4)
    g.add_edge('4043', '4273', 5)
    g.add_edge('4043', '4040', 6)
    g.add_edge('4051', '2827', 7)
    g.add_edge('4051', '4030', 8)
    g.add_edge('4051', '3180', 9)
    g.add_edge('4057', '3180', 1)
    g.add_edge('4057', '4032', 2)
    g.add_edge('4057', '4063', 3)
    g.add_edge('4063', '4057', 4)
    g.add_edge('4063', '2200', 5)
    g.add_edge('4063', '3127', 6)
    g.add_edge('4063', '4034', 7)
    g.add_edge('4262', '4263', 8)
    g.add_edge('4262', '3001', 9)
    g.add_edge('4263', '4264', 1)
    g.add_edge('4263', '4262', 2)
    g.add_edge('4263', '3002', 3)
    g.add_edge('4264', '4270', 4)
    g.add_edge('4264', '4266', 5)
    g.add_edge('4264', '4263', 6)
    g.add_edge('4264', '4324', 7)
    g.add_edge('4266', '4264', 8)
    g.add_edge('4266', '4040', 9)
    g.add_edge('4270', '4272', 1)
    g.add_edge('4270', '4264', 2)
    g.add_edge('4270', '4812', 3)
    g.add_edge('4272', '4273', 4)
    g.add_edge('4272', '4040', 5)
    g.add_edge('4272', '4270', 6)
    g.add_edge('4273', '4043', 7)
    g.add_edge('4273', '4272', 8)
    g.add_edge('4321', '4030', 9)
    g.add_edge('4321', '4335', 1)
    g.add_edge('4324', '3662', 2)
    g.add_edge('4324', '4034', 3)
    g.add_edge('4324', '4264', 4)
    g.add_edge('4335', '3662', 5)
    g.add_edge('4335', '4321', 6)
    g.add_edge('4812', '4270', 7)
    g.add_edge('4821', '3001', 9)

    g.printgraph()

    print('list')
    print([(v.get_distance(), v) for v in g])
    # startnode = input("Enter the origin SCAT Site:")
    # endnode = input("Enter the destination SCAT Site:")
    dijkstra(g, g.get_vertex('0970'), g.get_vertex('4270'))

    target = g.get_vertex('4270')
    path = [target.get_id()]
    shortest(target, path)
    print('The shortest path : %s' % (path[::-1]))



    





