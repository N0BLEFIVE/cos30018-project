# README #

### What is this repository for? ###

This repo contains assets for creating a traffic flow prediction system for Boroondara City that uses Machine Learning to predict traffic and estimate travel time.

### Source Code Guide
* GUIinterface : Contains the final application for this project
	* ApplicationEngine : contains refactored version of "TFP Graph Representation and Routing"
		* data : contains data depedencies of the application
		* models : contains CNN, GRU, and LSTM pretrained models (output from "Model Training.ipynb")
		* Edge.py : edge object of used in graphs (representation of streets/roads connecting vertices)
		* Vertex.py : vertex object of used in grpahs (represenation of scats sites/intersections)
		* Graph.py : graph objcet (representation of road networks)
		* RouteingTime.py : serves as final facade object for the overall Routing System 
	* NodeAndEdgeObjects :contains object assets for GUI
		* EdgePoint.py : behaviour for storing information regarding roads. Names  xy positions, colour and thickness
		* NodePoint.py : behaviour for scat sites. There is also information stored which help with edge Street associated
		* Routes.py : contains edge objects that are on the returned route
	* GenerateDataStructure.py : prepares the node and edge objects from the CSV data (SCATdata.csv)
	* GraphingData.py : controls the gui interface and user actions, links to the RouteingTime.py inside the application engine (Main GUI File)
	* SCATdata.csv : lists scat sites, long and lat values, the road name labels (used for GUI)
	* ShapeDrawer.py : responsible for drawing edges, nodes, and other visual components to the interface
* Models : Contains Models
	* CNN.h5 : Pretrained Convolutional Neural Network Model
	* GRU.h5 : Pretrained Gated Recurrent Units Model
	* LSTM.h5 : Pretrained Long-Short Term Memory Model
* TFP Experiments : Contains source code and data for model training and testing
	* data_ave : contains train and test datasets imputed with time specific traffic flow average 
	* data_zero : contains tran and test datasets imputed with zero values
	* Data Exploratuon.ipynb : notebook for preliminary inspection of dataset
	* Data Preparation (Cleaning and Split).ipynb : notebook for cleaning the dataset, imputation, and splitting to train and test data
	* Model Training.ipynb : notebook for training models
	* Model Testing.ipynb : notebook for testing and test runs of the models
	* Time Input.ipynb : notebook containing test functions for contstructing input data given a datetime string (used in the final application)
* TFP Graph Representation and Routing : contains source code for grpah representaion of road network that uses traffic prediction
	* data : contains data depedencies of the application
	* models : contains CNN, GRU, and LSTM pretrained models (output from "Model Training.ipynb")
	* Routing.py : test routing algorithm using simple graph without prediction components 
	* main.py : contains final source for graph representaition with routing logic that uses traffic prediction
* gmplot test : contains attempts to visualize points 