import pandas as pd
from pathlib import Path
import sys
import random

class NodeMapData:
    def __init__(self,WindowX,WindowY):
        self.LargestLong = None
        self.SmallestLong = None

        self.SmallestLat = None
        self.LargestLat = None

        self.Margin = 100
        self.X = WindowX - self.Margin
        self.Y = WindowY - self.Margin

        dir_path = Path(__file__).parents[0]
        dir_path = str(dir_path) + "\SCATdata.csv"

        newDF = pd.read_csv(dir_path)

        self.IsolatedScats = []

        self.LargestLong = float(newDF.iloc[0,2])
        self.LargestLat = float(newDF.iloc[0,3])

        self.SmallestLong = float(newDF.iloc[0,2])
        self.SmallestLat = float(newDF.iloc[0,3])

        self.ConnectedRoads = []
        for x in range(len(newDF) - 1):

            if newDF.iloc[x,1] != newDF.iloc[x + 1,1]:
                self.ConnectedRoads.append(str(newDF.iloc[x,1]))
            
            if x + 2 == len(newDF):
                Long = float(newDF.iloc[x,2])
                Lat = float(newDF.iloc[x,3])

                self.IsolatedScats.append([newDF.iloc[x,0],Long,Lat,self.ConnectedRoads])
                self.ConnectedRoads = []

                if Long > self.LargestLong:
                    self.LargestLong = Long
                if Long < self.SmallestLong:
                    self.SmallestLong = Long

                if Lat > self.LargestLat:
                    self.LargestLat = Lat
                if Lat < self.SmallestLat:
                    self.SmallestLat = Lat
                break

            if newDF.iloc[x,0] != newDF.iloc[x + 1,0]:
                Long = float(newDF.iloc[x,2])
                Lat = float(newDF.iloc[x,3])

                self.IsolatedScats.append([newDF.iloc[x,0],Long,Lat,self.ConnectedRoads])
                self.ConnectedRoads = []

                if Long > self.LargestLong:
                    self.LargestLong = Long
                if Long < self.SmallestLong:
                    self.SmallestLong = Long

                if Lat > self.LargestLat:
                    self.LargestLat = Lat
                if Lat < self.SmallestLat:
                    self.SmallestLat = Lat

                ####print(self.ConnectedRoads)

        ####print(self.IsolatedScats)

    def ReturnScatsData(self):
        for x in range(len(self.IsolatedScats)):
            self.IsolatedScats[x].append(self.ScaleLong(self.IsolatedScats[x][1]))
            self.IsolatedScats[x].append(self.ScaleLat(self.IsolatedScats[x][2]))
        return self.IsolatedScats

    def ScaleLat(self,x):
        Scale = (self.X-0)*(float(x)-self.SmallestLat)/(self.LargestLat - self.SmallestLat)
        return round(Scale + (self.Margin / 2))

    def ScaleLong(self,x):
        Scale = (self.Y-0)*(float(x)-self.SmallestLong)/(self.LargestLong - self.SmallestLong)
        return round(Scale + (self.Margin / 2))


