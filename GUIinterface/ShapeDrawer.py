from typing import Type
from NodeAndEdgeObjects.NodePoint import NodePoint as NP
from NodeAndEdgeObjects.EdgePoint import EdgePoint
from NodeAndEdgeObjects.Routes import RoutesObject
import PySimpleGUI as sg    
import re


class ShapeDrawer:
    def __init__(self,Canvas:Type[sg.Window]):
        self.NodeList = []
        self.EdgeList = []
        self.Routes = []
        self.Graph = Canvas
        self.CorrectNullConnections = [['VICTORIA','BARKERS'],['SWAN','RIVERSDALE'],['ARTERIAL','BURKE'],['MAROONDAH HWY','WHITEHORSE'],['BURWOOD','CHURCH'],['UNION','CANTERBURY'],['BURWOOD','CAMBERWELL'],['TRAFALGAR','STANHOPE'],['COTHAM','HIGH']]
        self.FromSelected = None
        self.ToSelected = None

    def AssociateStreets(self):
        for x in range(len(self.EdgeList)):
            ScatOne = self.EdgeList[x].ScattOne()
            ScatTwo = self.EdgeList[x].ScattTwo()

            NodeOnePotentualRoads = []
            NodeTwoPotentualRoads = []
            for y in range(len(self.NodeList)):
                
                if self.NodeList[y].CheckScatt(ScatOne) == True:
                    NodeOnePotentualRoads.extend(self.NodeList[y].ReturnStreetNames())
                if self.NodeList[y].CheckScatt(ScatTwo) == True:
                    NodeTwoPotentualRoads.extend(self.NodeList[y].ReturnStreetNames())
            
            if len(NodeOnePotentualRoads) < len(NodeTwoPotentualRoads):
                self.EdgeList[x].SetStreetBane(self.CommonRoads(NodeOnePotentualRoads,NodeTwoPotentualRoads,ScatOne,ScatTwo))
            else:
                self.EdgeList[x].SetStreetBane(self.CommonRoads(NodeTwoPotentualRoads,NodeOnePotentualRoads,ScatTwo,ScatOne))

    def CommonRoads(self,SmallestSelection,LargestSelection,ScatOne,ScatTwo):
            for z in range(len(SmallestSelection)):
                for q in range(len(LargestSelection)):
                    if SmallestSelection[z] == LargestSelection[q]:
                        return SmallestSelection[z]

            xPos = "None"
            yPos = "None"
            ##print(len(self.CorrectNullConnections))
            for h in range(len(self.CorrectNullConnections)):
                for WoW in range(len(SmallestSelection)):
                    for WwW in range(len(LargestSelection)):
                        if LargestSelection[WwW] == self.CorrectNullConnections[h][0] and SmallestSelection[WoW] == self.CorrectNullConnections[h][1]:
                            xPos = LargestSelection[WwW]
                            yPos = SmallestSelection[WoW]
                        elif LargestSelection[WwW] == self.CorrectNullConnections[h][1] and SmallestSelection[WoW] == self.CorrectNullConnections[h][0]:
                            yPos = LargestSelection[WwW]
                            xPos = SmallestSelection[WoW]

            if xPos == "None" or yPos == "None":
                print(ScatOne,SmallestSelection,ScatTwo,LargestSelection)
            else:
                return str(xPos) + " via " + str(yPos)
            

    def AddNode(self,ScatNumber,X,Y,StreetName,Long,Lat):
        self.NodeList.append(NP(ScatNumber,X,Y,StreetName,Long,Lat))

    def add_edge(self,NodeOne,NodeTwo):
        NodeOne = int(NodeOne)
        NodeTwo = int(NodeTwo)
        
        NodeOneX = None
        NodeOneY = None
        ScatOne = int(NodeOne)

        NodeTwoX = None
        NodeTwoY = None
        ScatTwo = int(NodeTwo)

        for x in range(len(self.NodeList)):
            if NodeOne == self.NodeList[x].ReturnScatNumber():
                NodeOneX = self.NodeList[x].ReturnX()
                NodeOneY = self.NodeList[x].ReturnY()
                ScatOne = self.NodeList[x].ReturnScatNumber()

            if NodeTwo == self.NodeList[x].ReturnScatNumber():
                NodeTwoX = self.NodeList[x].ReturnX()
                NodeTwoY = self.NodeList[x].ReturnY()
                ScatTwo = self.NodeList[x].ReturnScatNumber()
        Edge = EdgePoint(NodeOneX,NodeOneY,NodeTwoX,NodeTwoY,ScatOne,ScatTwo)
        self.EdgeList.append(Edge)

    def FindMidPoint(self,x1,x2,y1,y2):
        return [(x1+x2)/2,(y1+y2)/2]

    def DrawEdges(self):
        for x in range(len(self.EdgeList)):
            if self.EdgeList[x].ReturnStreetNane() != None:
                self.Graph.DrawLine((self.EdgeList[x].GiveYone(),self.EdgeList[x].GiveXone()), (self.EdgeList[x].GiveYtwo(),self.EdgeList[x].GiveXtwo()), color = "black", width = self.EdgeList[x].GetThickness()) 
                
            TextXY = self.FindMidPoint(self.EdgeList[x].GiveYone(),self.EdgeList[x].GiveYtwo(),self.EdgeList[x].GiveXone(),self.EdgeList[x].GiveXtwo())
                
            font = ("Courier New", 7)
            self.Graph.DrawText(self.EdgeList[x].ReturnStreetNane(), (TextXY[0],TextXY[1]+5), font=font,color='red')
        
    def DrawNodes(self):
        for x in range(len(self.NodeList)):
            font = ("Courier New", 8)
            self.Graph.DrawText(self.NodeList[x].ReturnScatNumber(), (self.NodeList[x].ReturnY(),self.NodeList[x].ReturnX()+20), font=font,color='blue')
            self.Graph.DrawCircle((self.NodeList[x].ReturnY(),self.NodeList[x].ReturnX()), 7, fill_color='orange',line_color='white') 

        if self.ToSelected != None: 
            self.Graph.DrawCircle((self.ToSelected.ReturnY(),self.ToSelected.ReturnX()), 7, fill_color='blue',line_color='white') 

        if self.FromSelected != None: 
            self.Graph.DrawCircle((self.FromSelected.ReturnY(),self.FromSelected.ReturnX()), 7, fill_color='blue',line_color='white') 

    def ReturnNodes(self):
        return self.NodeList
    
    def FindClosestNode(self,XnY,TOnFrom):
        closest = None
        for Num in range(len(self.NodeList)):
            if self.NodeList[Num].Distance(XnY) != None:
                closest = self.NodeList[Num].Distance(XnY)
        
        if closest != None:   
            if TOnFrom == "To":
                self.ToSelected = closest
            else:
                self.FromSelected = closest
            return closest.ReturnScatNumber()

    def CheckNode(self,Check):
        Check = int(Check)
        for NodeNumber in range(len(self.NodeList)):
            if Check == self.NodeList[NodeNumber].ReturnScatNumber():
                return True
        return False

    def BuildRoutes(self, RouteArray):
        Routes = RouteArray
        self.Routes = []

        for Route in range(len(Routes)):
            self.Routes.append(RoutesObject(Routes[Route],self.EdgeList,self.Graph))

    def DisplayRoute(self,RouteNum):
        self.Routes[RouteNum].HighlightRoute()

    def RouteQTY(self):
        return len(self.Routes)