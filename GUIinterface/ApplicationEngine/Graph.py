import math
import heapq
import sys
import csv

import ast
from PySimpleGUI.PySimpleGUI import Output
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from keras.models import load_model
from keras.utils.vis_utils import plot_model
import sklearn.metrics as metrics
from pathlib import Path

from ApplicationEngine.Vertex import Vertex
from ApplicationEngine.Edge import Edge
from ApplicationEngine.TrafficPrediction import TrafficPrediction

class Graph:
    def __init__(self):
        self.adjacency_list = {}
        self.num_vertices=0


    # load coordinates from csv file containg coordinates
    def load_coordinates(self):
        # open csv file
        dir_path = Path(__file__).parents[1]
        dir_path = str(dir_path) + "\ApplicationEngine\data\scat_coords.csv"

        with open(dir_path, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            # map csv file to correct sites
            for row in csv_reader:
                if row['SCATS Number'] in self.adjacency_list:
                    site = self.get_vertex(row['SCATS Number'])
                    site.set_lat(float(row['NB_LATITUDE']))
                    site.set_long(float(row['NB_LONGITUDE']))
            # update distance values for edge
            self.recalculate_edge_distances()
            
    
    def recalculate_edge_distances(self):
        # iterate over adjacency list
        for node in self.adjacency_list:
            # iterate over edges of each vertex
            for edge in self.adjacency_list[node].edges.values():
                # update distance values
                edge.recalculate_distance()


    def add_node(self, node):
        # increment number of vertices
        self.num_vertices = self.num_vertices + 1
        # create new vertex given scat site number
        new_vertex = Vertex(node)
        # add vertex to adjacency list
        self.adjacency_list[node] = new_vertex
        # return newly create vertex
        return new_vertex

    def add_edge(self,detector,scat_start, scat_end):
        # check if either vertex exist in adjacency list
        # add if not
        if scat_start not in self.adjacency_list:
            self.add_node(scat_start)
        if scat_end not in self.adjacency_list:
            self.add_node(scat_end)

        # if it exist, instantiate edge
        # add ass edge to the starting vertex
        edge = Edge(detector,self.get_vertex(scat_start),self.get_vertex(scat_end))
        self.adjacency_list[scat_start].add_edge(edge)

    def add_complete_edge(self,scat_start, edge):
        # same as add edge, but for instantiated edges
        self.adjacency_list[scat_start].add_edge(edge)
            
    def load_predictions(self, prediction_df):
        # iterate over adjacency list
        for node in self.adjacency_list:
            # iterate over edges of each vertex
            for edge in self.adjacency_list[node].edges.values():
                # if the detector is in the prediction dataframe
                # set traffic(flow) value based on prediction
                if edge.detector in prediction_df:
                    edge.set_traffic(float(prediction_df[edge.detector]))
        # update travel time for edges
        self.recalculate_travel_time()

    def recalculate_travel_time(self):
        # iterate over adjacency list
        for node in self.adjacency_list:
            # iterate over each edge for each vertex
            for edge in self.adjacency_list[node].edges.values():
                # update travel time values for each edge
                edge.recalculate_travel()

    def print_predictions(self):
        # print adjacency list with predictions mapped
        print("predictions")
        for node in self.adjacency_list:
            for edge in self.adjacency_list[node].edges.values():
                print(f"[{node}]", end='')
                print(f"(detector \"{edge.detector}\", end:{edge.end.scat_num}, traffic flow: {edge.flow:.2f} veh/15mins, travel distance: {edge.distance:.2f} km,travel speed: {(edge.travel_speed*60*60):.2f} km/hr, travel time: {edge.travel_time:.2f} secs) \n", end='')
            print()
    
    def print_adjacency_list(self):
        # print adjacency list
        print(self.adjacency_list)
  
    def print_graph(self):
        # print graph
        print("[start](end,detector,distance) ...")
        for node in self.adjacency_list:
            print(f"[{node}]", end='')
            for edge in self.adjacency_list[node].edges.values():
                print(f"(detector \"{edge.detector}\", end:{edge.end}, distance {edge.distance:0.2f} km) ", end='')
            print()
    
    def get_vertex(self, scat_num):
        if scat_num in self.adjacency_list:
            return self.adjacency_list[scat_num]
        else:
            return None

    def get_edges(self, scat_num):
        return self.adjacency_list[scat_num].edges

    def get_edge(self, start, end):
        return self.adjacency_list[start].get_edge(end)

    def remove_edge(self, start, end):
        for n in self.adjacency_list[start].edges:
            print (n)
        print("after ")
        self.adjacency_list[start].remove_edge(end)
        for n in self.adjacency_list[start].edges:
            print (n)

    def flush(self):
        # overwrite values assigned during dijkstra's algorithm 
        # used for modiying the graph for generating other routes
        for node in self.adjacency_list:
            self.adjacency_list[node].set_current_cost(sys.maxsize)
            self.adjacency_list[node].visited = False

    def dijkstra(self, start, target):
        print('''Dijkstra's shortest path''')
        # Set the distance for the start node to zero 
        start.set_current_cost(0)

        # Put tuple pair into the priority queue
        unvisited_queue = [(v.get_current_cost(), v) for v in self.adjacency_list.values()]
        heapq.heapify(unvisited_queue)

        while len(unvisited_queue):
            # Pops a vertex with the smallest path cost 
            uv = heapq.heappop(unvisited_queue)
            current = uv[1]
            current.set_visited()

            # for next in v.adjacent:
            for next in current.edges.values():
                # if visited, skip
                if next.end.visited:
                    continue
                
                # calculate new cost of path, get current standing cost and add new cost if edge/path is taken
                new_cost = current.get_current_cost() + current.get_travel_time(next.end.scat_num)

                # if new cost is less than current exiting path to the next vertex, then update
                if new_cost < next.end.get_current_cost():
                    print("updated : current = {current} next = {next} new cost = {distance} vs current cost = {prev}".format(current = current.get_id(), next = next.end.get_id(), distance= new_cost, prev = next.end.get_current_cost()))
                    next.end.set_current_cost(new_cost)
                    next.end.set_previous(current)
                    
                else:
                    print("not updated : current = {current} next = {next} new cost = {distance} vs current cost = {prev}".format(current = current.get_id(), next = next.end.get_id(), distance= new_cost, prev = next.end.get_current_cost()))

            # Rebuild heap
            # 1. Pop every item
            while len(unvisited_queue):
                heapq.heappop(unvisited_queue)
            # 2. Put all vertices not visited into the queue
            unvisited_queue = [(v.get_current_cost(), v) for v in self.adjacency_list.values() if not v.visited ]
            heapq.heapify(unvisited_queue)

        # generate list of steps (path) 
        path = [target.get_id()]
        self.shortest(target, path)
        
        # flip from target -> start to start -> target
        # place path in tuple with path cost
        return (path[::-1],target.get_current_cost())

    def shortest(self, v, path):
        # make shortest path from vertex.previous 
        if v.previous:
            path.append(v.previous.get_id())
            self.shortest(v.previous, path)
        return

    def shortest_paths(self, start, end, model = "lstm", start_date = "2006-10-29 09:00:00"):
        # instantiate predictor
        predictor = TrafficPrediction()
        # get predictor
        predictions = predictor.predict(start_date, model)
        # assign predictions to correct edges bsaed on detector value
        self.load_predictions(predictions)

        # initialize list
        paths = []

        # generate shortest path
        shortest_path = self.dijkstra(self.get_vertex(start), self.get_vertex(end))

        # append shortest path as first path in paths
        paths.append(shortest_path)

        # iterate over steps taken by shortest path
        for i in range(1,len(shortest_path[0])):
            print(shortest_path[0][i-1], shortest_path[0][i])

            # flush : set vertices to unvisited and max current travel cost
            self.flush()

            # get edge to remove (edge/path taken by shortest path)
            re = self.get_edge(shortest_path[0][i-1], shortest_path[0][i])

            # remove edge
            self.remove_edge(re.start.get_id(),re.end.get_id())

            # get alternative shortest path given modified graph and append to paths
            paths.append(self.dijkstra(self.get_vertex(start), self.get_vertex(end)))

            # add removed edge
            self.add_complete_edge(re.start.get_id(), re)

        # sort paths by shortest travel time
        paths.sort(key = lambda x: x[1]) 

        return paths