import sys

class Vertex:
    def __init__(self, scat_num, lat = 0.0, long = 0.0):
        self.scat_num = scat_num
        self.lat = lat
        self.long = long
        # initialise current travel cost to max (used in dijsktra's algo)
        self.current_travel_cost = sys.maxsize
        self.edges = {}
        # auxiliary variables used in dijsktra's algo to keep track of path finding process
        self.visited = False
        self.previous = None

    def __str__(self):
        return "{id}, x = {x:0.2f}, y = {y:0.2f}".format(id = self.scat_num, x= self.lat, y=self.long)

    def __repr__(self):
        return str(self)

    def __eq__(self, scat_num):
        return self.scat_num == scat_num

    def __hash__(self):
        return hash(str(self.scat_num))
    
    def __lt__(self, other):
        return self.current_travel_cost < other.current_travel_cost

    def set_lat(self, lat):
        self.lat = lat

    def get_lat(self):
        return self.lat

    def set_long(self, long):
        self.long = long

    def get_long(self):
        return self.long

    def get_id(self):
        return self.scat_num
    
    def set_current_cost(self, cost):
        self.current_travel_cost = cost

    def get_current_cost(self):
        return self.current_travel_cost

    def add_edge(self, edge):
        self.edges[edge.end.scat_num] = edge
    
    def get_edge(self, edge):
        return self.edges[edge]

    def remove_edge(self, edge):
        self.edges.pop(edge)

    def set_previous(self, prev):
        self.previous = prev

    def set_visited(self):
        self.visited = True

    def get_travel_time(self, adjacent_site):
        return self.edges[adjacent_site].get_travel_time()

