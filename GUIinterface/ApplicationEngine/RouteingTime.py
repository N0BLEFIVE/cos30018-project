import math
import heapq
import sys
import csv

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from keras.models import load_model
from keras.utils.vis_utils import plot_model
import sklearn.metrics as metrics
from pathlib import Path

from .Graph import Graph
from .TrafficPrediction import TrafficPrediction


class RouteingTime:
    def __init__(self):
        self.graph = Graph()
        self.graph.add_edge("WARRIGAL_RD N of HIGH STREET_RD",'0970', '3685')
        self.graph.add_edge("WARRIGAL_RD W of HIGH STREET_RD",'0970', '2846')
        self.graph.add_edge("WARRIGAL_RD N of TOORAK_RD",'2000', '3682')
        self.graph.add_edge("WARRIGAL_RD S of BURWOOD_HWY",'2000', '3685')
        self.graph.add_edge("TOORAK_RD W of WARRIGAL_RD",'2000', '4043')
        self.graph.add_edge("MAROONDAH_HWY W of UNION_RD",'2200', '4063')
        self.graph.add_edge("UNION_RD S of MAROONDAH_HWY",'2200', '3126')
        self.graph.add_edge("PRINCESS_ST S of CHANDLER_HWY",'2820', '3662')
        self.graph.add_edge("BURKE_RD S of EASTERN_FWY",'2825', '4030')
        self.graph.add_edge("BULLEEN_RD S of EASTERN_FWY_W_BD_RAMPS",'2827', '4051')
        self.graph.add_edge("HIGH_ST E OF S.E.ARTERIAL",'2846', '0970')
        self.graph.add_edge("WILLS_ST NW OF HIGH_ST",'2846', '4043')
        self.graph.add_edge("CHURCH_ST SW of BARKERS_RD",'3001', '4262')
        self.graph.add_edge("HIGH_ST NE of BARKERS_RD",'3001', '3662')
        self.graph.add_edge("BARKERS_RD E of HIGH_ST",'3001', '3002')
        self.graph.add_edge("BARKERS_RD W of CHURCH_ST",'3001', '4821')
        self.graph.add_edge("BARKERS_RD W of DENMARK_ST",'3002', '3001')
        self.graph.add_edge("BARKERS_RD E of DENMARK_ST",'3002', '4035')
        self.graph.add_edge("DENMARK_ST N of BARKERS_RD",'3002', '3662')
        self.graph.add_edge("POWER_ST S of BARKERS_RD",'3002', '4263')
        self.graph.add_edge("BURKE_RD N of CANTERBURY_RD",'3120', '4035')
        self.graph.add_edge("CANTERBURY_RD E of BURKE_RD",'3120', '3122')
        self.graph.add_edge("BURKE_RD S of CANTERBURY_RD",'3120', '4040')
        self.graph.add_edge("CANTERBURY_RD W of STANHOPE_GV",'3122', '3120')
        self.graph.add_edge("CANTERBURY_RD E of STANHOPE_GV",'3122', '3127')
        self.graph.add_edge("STANHOPE_GV S of CANTERBURY_RD",'3122', '3804')
        # there was no connection from 3126 to 2200 since it didnt have a north detector
        #there was no scat site adjacent to the east detector 
        self.graph.add_edge("WARRIGAL_RD S of CANTERBURY_RD",'3126', '3682')
        self.graph.add_edge("CANTERBURY_RD W of WARRIGAL_RD",'3126', '3127')
        self.graph.add_edge("BALWYN_RD N of CANTERBURY_RD",'3127', '4063')
        self.graph.add_edge("CANTERBURY_RD W of BALWYN_RD",'3127', '3122')
        self.graph.add_edge("CANTERBURY_RD E of BALWYN_RD",'3127', '3126')
        self.graph.add_edge("DONCASTER_RD W of BALWYN_RD",'3180', '4051')
        self.graph.add_edge("BALWYN_RD S of DONCASTER_RD",'3180', '4057')
        #there was no connection from 3662 to 3002 since it didnt have a south detector
        self.graph.add_edge("HIGH_ST SW of DENMARK_ST",'3662', '3001')
        self.graph.add_edge("PRINCESS_ST N of HIGH_ST",'3662', '2820')
        self.graph.add_edge("HIGH_ST NE of PRINCESS_ST",'3662', '4335')
        #there was no connection from 3662 to 4324 unless if you use the same road as above, then there will an issue will loop iteration?
        #graph.add_edge("a",'3662', '4324')
        self.graph.add_edge("WARRIGAL_RD N OF RIVERSDALE_RD",'3682', '3126')
        self.graph.add_edge("RIVERSDALE_RD W OF WARRIGAL_RD",'3682', '3804')
        self.graph.add_edge("WARRIGAL_RD S OF RIVERSDALE_RD",'3682', '2000')
        #no east detector labelled since there is no scat site on east of 3682
        self.graph.add_edge("WARRIGAL_RD N of HIGHBURY_RD",'3685', '2000')
        self.graph.add_edge("WARRIGAL_RD S of HIGHBURY_RD",'3685', '0970')
        #no east detector labelled since there is no scat site on east of 3685
        self.graph.add_edge("RIVERSDALE_RD E of TRAFALGAR_RD",'3804', '3682')
        self.graph.add_edge("TRAFALGAR_RD S of RIVERSDALE_RD",'3804', '3812')
        self.graph.add_edge("RIVERSDALE_RD W of TRAFALGAR_RD",'3804', '4040')
        self.graph.add_edge("TRAFALGAR_RD NE of CAMBERWELL_RD",'3812', '3804')
        self.graph.add_edge("CAMBERWELL_RD NW of TRAFALGAR_RD",'3812', '4040')
        self.graph.add_edge("TRAFALGAR_RD N of RIVERSDALE_RD",'3804', '3122')
        # west and southeast detectors not labelled cus it does appear to have connecting scat sites- inquire
        #no node for northeast conencting from 4030 to 4051 -  it is a one way
        #graph.add_edge("a",'4030', '4051')
        self.graph.add_edge("BURKE_RD S of DONCASTER_RD",'4030', '4032')
        #no detector for the connecting to north from 4030 to 2825
        #graph.add_edge("a",'4030', '2825')
        self.graph.add_edge("HIGH_ST SW of BURKE_RD",'4030', '4321')# double check this detector
        self.graph.add_edge("BURKE_RD N of HARP_RD",'4032', '4030')
        self.graph.add_edge("HARP_RD W of BURKE_RD",'4032', '4321')
        self.graph.add_edge("BELMORE_RD E of BURKE_RD",'4032', '4057')
        self.graph.add_edge("BURKE_RD S of HARP_RD",'4032', '4034')
        self.graph.add_edge("WHITEHORSE_RD E OF BURKE_RD",'4034', '4063')
        self.graph.add_edge("BURKE_RD N OF WHITEHORSE_RD",'4034', '4032')
        self.graph.add_edge("BURKE_RD S OF WHITEHORSE_RD",'4034', '4035')
        self.graph.add_edge("COTHAM_RD W OF BURKE_RD",'4034', '4324')
        self.graph.add_edge("BARKERS_RD W of BURKE_RD",'4035', '3002')
        self.graph.add_edge("BURKE_RD S of BARKERS_RD",'4035', '3120')
        self.graph.add_edge("BURKE_RD N of MONT ALBERT_RD",'4035', '4034')
        self.graph.add_edge("RIVERSDALE_RD W of BURKE_RD",'4040', '4272')
        self.graph.add_edge("CAMBERWELL_RD NW of BURKE_RD",'4040', '4266')
        self.graph.add_edge("BURKE_RD N of RIVERSDALE_RD",'4040', '3120')
        self.graph.add_edge("RIVERSDALE_RD E of BURKE_RD",'4040', '3804')
        self.graph.add_edge("CAMBERWELL_RD SE of BURKE_RD",'4040', '3812')
        self.graph.add_edge("BURKE_RD S of RIVERSDALE_RD",'4040', '4043')
        self.graph.add_edge("BURKE_RD S of TOORAK_RD",'4043', '2846')
        self.graph.add_edge("TOORAK_RD E of BURKE_RD",'4043', '2000')
        self.graph.add_edge("TOORAK_RD W OF BURKE_RD",'4043', '4273')
        self.graph.add_edge("BURKE_RD N of TOORAK_RD",'4043', '4040')
        self.graph.add_edge("SEVERN_ST S of DONCASTER_RD",'4051', '2827')
        self.graph.add_edge("DONCASTER_RD SW of SEVERN_ST",'4051', '4030')
        self.graph.add_edge("DONCASTER_RD E of BULLEEN_RD",'4051', '3180')
        self.graph.add_edge("BALWYN_RD N OF BELMORE_RD",'4057', '3180')
        self.graph.add_edge("BALWYN_RD S OF BELMORE_RD",'4057', '4063')
        self.graph.add_edge("BELMORE_RD W OF BALWYN_RD",'4057', '4032')
        self.graph.add_edge("BALWYN_RD N OF WHITEHORSE_RD",'4063', '4057')
        self.graph.add_edge("WHITEHORSE_RD E OF BALWYN_RD",'4063', '2200')
        self.graph.add_edge("BALWYN_RD S OF WHITEHORSE_RD",'4063', '3127')
        self.graph.add_edge("WHITEHORSE_RD W OF BALWYN_RD",'4063', '4034')

        self.graph.add_edge("BRIDGE_RD SW of BURWOOD_RD",'4262', '4263') #not sure
        self.graph.add_edge("BRIDGE_RD SW of BURWOOD_RD",'4262', '3001') #Ithink both will have the same detector since theres only one
        self.graph.add_edge("BURWOOD_RD E of POWER_ST",'4263', '4264')
        self.graph.add_edge("BURWOOD_RD W of POWER_ST",'4263', '4262')
        self.graph.add_edge("POWER_ST N of BURWOOD_RD",'4263', '3002')
        self.graph.add_edge("GLENFERRIE_RD S of BURWOOD_RD",'4264', '4270')
        self.graph.add_edge("BURWOOD_RD E of GLENFERRIE_RD",'4264', '4266')
        self.graph.add_edge("BURWOOD_RD W of GLENFERRIE_RD",'4264', '4263')
        self.graph.add_edge("GLENFERRIE_RD N of BURWOOD_RD",'4264', '4324')
        #north and south unimportant for connecting from 4266 
        self.graph.add_edge("BURWOOD_RD W of AUBURN_RD",'4266', '4264')
        self.graph.add_edge("BURWOOD_RD E of AUBURN_RD",'4266', '4040')
        self.graph.add_edge("RIVERSDALE_RD E of GLENFERRIE_RD",'4270', '4272')
        self.graph.add_edge("GLENFERRIE_RD N of RIVERSDALE_RD",'4270', '4264')
        self.graph.add_edge("RIVERSDALE_RD W of GLENFERRIE_RD",'4270', '4812')
        #north unimportant for connectiong from 4272
        self.graph.add_edge("TOORONGA_RD S of RIVERSDALE_RD",'4272', '4273')
        self.graph.add_edge("RIVERSDALE_RD E of TOORONGA_RD",'4272', '4040')
        self.graph.add_edge("RIVERSDALE_RD W of TOORONGA_RD",'4272', '4270')
        self.graph.add_edge("TOORAK_RD E of TOORONGA_RD",'4273', '4043')
        self.graph.add_edge("TOORONGA_RD N of TOORAK_RD",'4273', '4272')
        self.graph.add_edge("HIGH_ST NE OF HARP_ST",'4321', '4030')
        self.graph.add_edge("HIGH_ST SW OF HARP_ST",'4321', '4335')
        #added the next two connectors below
        self.graph.add_edge("VALERIE_ST W OF HIGH_ST",'4321', '2820')
        self.graph.add_edge("HARP_RD E OF HIGH_ST",'4321', '4032')
        self.graph.add_edge("COTHAM_RD W OF GLENFERRIE_RD",'4324', '3662')
        self.graph.add_edge("COTHAM_RD E OF GLENFERRIE_RD",'4324', '4034')
        self.graph.add_edge("GLENFERRIE_RD S OF COTHAM_RD",'4324', '4264')
        #theres a duplication in the scat site location ,One might need to be changed to NW instead of NE, ending 3662 would be NW i assume
        self.graph.add_edge("HIGH_ST NE of CHARLES_ST",'4335', '3662')
        self.graph.add_edge("HIGH_ST NE of CHARLES_ST",'4335', '4321')
        #SW and NE unimport for connecting 4812 - DOUBLE CHECK
        self.graph.add_edge("MADDEN_GV S OF SWAN_ST",'4812', '4270')
        #N,S and W unimportant for connecting 4821
        self.graph.add_edge("VICTORIA_ST E OF BURNLEY_ST",'4821', '3001')
        self.graph.load_coordinates()

    def GetRoute(self,ScatInputOne,ScatInputTwo,AlgoType = "lstm",DateTime = "2006-10-29 09:00:00"):
        self.predictor = TrafficPrediction()
        return self.graph.shortest_paths(ScatInputOne, ScatInputTwo, AlgoType, DateTime)
    
    