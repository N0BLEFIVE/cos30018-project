import math
import heapq
import sys
import csv

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from keras.models import load_model
from keras.utils.vis_utils import plot_model
import sklearn.metrics as metrics
from pathlib import Path

class TrafficPrediction:
    def __init__(self):
        dir_path = Path(__file__).parents[1]
        BasePath = str(dir_path) + "\ApplicationEngine\models"

        # load trained models
        self.lstm = load_model(BasePath + '\LSTM.h5')
        self.gru = load_model(BasePath +'\GRU.h5')
        self.cnn = load_model(BasePath + '\CNN.h5')
        self.lags = 12

    def process_input(self, start_datetime, end = 0.0):
        dir_path = Path(__file__).parents[1]
        dir_path = str(dir_path) + "\ApplicationEngine\data"

        # read train and test datasets and fill empty by zero
        # note : historical datasets are precleaned, hence no existing cells with empty values
        train_df = pd.read_csv(dir_path+'\Train.csv', encoding='utf-8').fillna(0)
        test_df = pd.read_csv(dir_path+"\Test.csv", encoding='utf-8').fillna(0)

        # keep list of headers (detectors of edges)
        column_names = test_df.columns.tolist()

        # create new dataframe for getting index given date
        datetime_label = pd.DataFrame(test_df["Date"], columns = ["Date"])
        o = datetime_label.index[datetime_label['Date'] == start_datetime]

        # get data input 
        # get traffic data from lags before prediction to time of prediction
        test_df = test_df[o[0]-self.lags:o[0]+1]
        
        # instantiate scaler
        scaler = MinMaxScaler()
        # fit scaler to same dataset used for training models
        scaler.fit(train_df.loc[:, train_df.columns != 'Date'])
        
        # scale dataset
        scaled_test_df = test_df
        scaled_test_df.loc[:, test_df.columns != 'Date'] = scaler.transform(test_df.loc[:, test_df.columns != 'Date'])
        
        # get values
        scaled_test_df_values = scaled_test_df.loc[:, scaled_test_df.columns != 'Date'].values

        # create features and labels referencing lags
        test = []

        for i in range(self.lags, len(scaled_test_df_values)):
            test.append(scaled_test_df_values[i - self.lags: i + 1])

        test = np.array(test)

        # get all values execpt for last value as features
        X_test = test[:, :-1]

        # get last value which serves as labels
        y_test = test[:, -1]

        # return features, labels, scaler used (for converting to correct values), column header (for mapping predictions)
        return X_test, y_test, scaler, column_names

    def predict(self, start_datetime, model_to_use = "lstm"):
        # assign model to be used based on parameter
        if model_to_use == "lstm": 
            model = self.lstm
        if model_to_use == "gru": 
            model = self.gru
        if model_to_use == "cnn": 
            model = self.cnn

        # preprocess data input
        X_test, y_test, scaler, column_names = self.process_input(start_datetime)

        # predict values
        predicted = model.predict(X_test)
        # inverse the transform using scaler
        predicted = scaler.inverse_transform(predicted)

        # return predictions as df
        prediction_df = pd.DataFrame(predicted,columns=column_names[1:])
        return prediction_df
