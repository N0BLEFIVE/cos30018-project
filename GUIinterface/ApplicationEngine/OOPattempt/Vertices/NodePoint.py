class SimpleNode():
    def __init__(self, scat_num, long, lat):
        self.long = long
        self.lat = lat

        self.scat_num = scat_num

    def set_lat(self, lat):
        self.lat = lat

    def get_lat(self):
        return self.lat

    def set_long(self, long):
        self.long = long

    def get_long(self):
        return self.long

    def get_id(self):
        return self.scat_num
