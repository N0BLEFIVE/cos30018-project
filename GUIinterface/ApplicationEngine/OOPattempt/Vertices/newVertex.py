from .NodePoint import SimpleNode
import math
import sys
from .DrawablePoint import NodePoint

class Vertex(SimpleNode):
    def __init__(self, scat_num, lat = 0.0, long = 0.0):
        super().__init__(scat_num, long, lat)

        # initialise current travel cost to max (used in dijsktra's algo)
        self.current_travel_cost = sys.maxsize
        self.edges = {}
        # auxiliary variables used in dijsktra's algo to keep track of path finding process
        self.visited = False
        self.previous = None

        # auxiliary variables
        self.distanceToEnd = None
        self.Ascore = float('inf')

    def setAstarScore(self,newScore):
            self.Ascore = newScore
    
    def getAstarScore(self):
        if (self.Ascore == None):
            return False
        else:
            return self.Ascore

    def __str__(self):
        return "{id}, x = {x:0.2f}, y = {y:0.2f}".format(id = self.scat_num, x= self.lat, y=self.long)

    def __repr__(self):
        return str(self)

    def __eq__(self, scat_num):
        return self.scat_num == scat_num

    def __hash__(self):
        return hash(str(self.scat_num))
    
    def __lt__(self, other):
        return self.current_travel_cost < other.current_travel_cost
    
    def set_current_cost(self, cost):
        self.current_travel_cost = cost

    def get_current_cost(self):
        return self.current_travel_cost

    def add_edge(self, edge):
        self.edges[edge.end.scat_num] = edge

    def CalculateDistance(self, EndPoint):
        EndPoint = self.calculateDistance(self.long,self.lat,EndPoint.get_lat(), EndPoint.get_lat())
        self.distanceToEnd = EndPoint
        return self.distanceToEnd

    def calculateDistance(self,x1,y1,x2,y2):
        dist = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
        return dist
    
    def get_edge(self, edge):
        return self.edges[edge]

    def return_edge(self):
        return self.edges

    def remove_edge(self, edge):
        self.edges.pop(edge)

    def set_previous(self, prev):
        self.previous = prev

    def set_visited(self):
        self.visited = True

    def get_travel_time(self, adjacent_site):
        return self.edges[adjacent_site].get_travel_time()

