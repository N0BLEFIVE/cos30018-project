
import re

from geopy.distance import Distance
import math 
from .NodePoint import SimpleNode

class NodePoint(SimpleNode):
    def __init__(self,ScatNumber,Long,Lat):
        super().__init__(ScatNumber, Long, Lat)
        self.ScatNumber = ScatNumber
        self.neighbours = []
        
        self.X = None
        self.Y = None
        
        self.FinalStreetList = []


        #for x in range(len(self.Streets)):
        #    tempArray = re.split(" of | OF ",str(self.Streets[x]))
        #    for y in range(len(tempArray)):
#
#                tempArray[y] = tempArray[y].upper()
#                tempArray[y] = re.sub('STREET', '_ST', tempArray[y])
#                tempArray[y] = re.sub('_', ' ', tempArray[y])
#                tempArray[y] = re.sub(r'\b\w{1,2}\b', '', tempArray[y])
#                tempArray[y] = tempArray[y].replace(".."," ")
#
#                tempArray[y] = tempArray[y].strip()
#                self.FinalStreetList.append(tempArray[y])

#        self.FinalStreetList = list(dict.fromkeys(self.FinalStreetList))

    def ReturnLong(self):
        return self.long

    def ReturnLat(self):
        return self.lat

    def ReturnStreetNames(self):
        return self.FinalStreetList

    def PreProcessedStreets(self):
        return self.Streets

    def ReturnXY(self):
        return [self.X,self.Y]

    def ReturnX(self):
        return self.X

    def ReturnY(self):
        return self.Y

    def add_edge(self,ScatNumber):
        self.neighbours.append(ScatNumber)

    def CheckEdge(self,ScatNumber):
        for x in range(len(self.neighbours)):
            if self.neighbours[x] == ScatNumber:
                return True
            else:
                return None

    def CheckScatt(self,CheckScatt):
        if CheckScatt == self.ScatNumber:
            return True
        else:
            return False

    def Distance(self,XnY):
        Xinput = XnY[1]
        Yinput = XnY[0]

        distance = self.calculateDistance(Xinput,Yinput,self.ReturnX(), self.ReturnY())
        if distance < 15:
            return self

    def calculateDistance(self,x1,y1,x2,y2):
        dist = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
        return dist
        