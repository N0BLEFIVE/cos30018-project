import math
import heapq
import sys
import csv

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from keras.models import load_model
from keras.utils.vis_utils import plot_model
import sklearn.metrics as metrics
from pathlib import Path

class Edge:
    def __init__(self, detector, start, end,  flow = 0.0):
        self.detector = detector
        self.start = start
        self.end = end
        self.flow = flow
        self.distance = self.calculate_distance(start, end)
        self.density = 0
        self.travel_speed = 0
        self.travel_time = 30

        # in the case that vertices are complete with longitude and latitude values
        # initialize density, travel speed, and travel time on instantiattion of object
        if (self.distance != 0):
            self.recalculate_travel()

    def __str__(self):
        return "({start}, {end}, {distance})".format(start = self.start, end = self.end, distance = self.distance)

    def __repr__(self):
        return str(self)
    
    def __lt__(self, other):
        return self.travel_time < other.travel_time

    def calculate_distance(self, start, end):
        # calculate distance using coordinates as input for distance formula
        # 1 deg latitude/longitude is 111 km
        return math.sqrt( ((end.lat-start.lat)**2)+((end.long-start.long)**2) ) * 111

    def set_traffic(self, flow):
        self.flow = flow

    def get_traffic(self):
        return self.flow

    def set_distance(self, distance):
        self.distance = distance
        
    def get_distance(self):
        return self.distance

    def set_start_coordinates(self, lat,long):
        self.start.set_lat(lat)
        self.start.set_log(long)

    def set_end_coordinates(self, lat,long):
        self.end.set_lat(lat)
        self.end.set_log(long)
    
    def recalculate_distance(self):
        # same as calculate distance but does not require input
        self.distance = self.calculate_distance(self.start, self.end)

    def recalculate_density(self):
        # calculate density under assumption that traffic flow is uniformally distributed
        # over the 15 min interval 
        self.density = (self.flow/15)/self.distance 

    def recalculate_travel(self):
        # update value for density
        self.recalculate_density()
        if self.density == 0:
            self.travel_time = 30
        else:
            # travel speed in km per second
            self.travel_speed = (self.flow/self.density)/(15*60)
            # if travel speed is faster than 60 km per hour (or 1/60th of a km per second)
            if self.travel_speed > 60/(60*60):
                # set travel speed to 60 km per hour (or 1/60th of a km per second)
                self.travel_speed = 60/(60*60)
            # set additional of 30 for intersection passed
            self.travel_time = (self.distance/self.travel_speed) + 30 

    def get_travel_time(self):
        return self.travel_time

  
