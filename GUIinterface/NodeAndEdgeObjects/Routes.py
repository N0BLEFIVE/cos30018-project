class RoutesObject:
    def __init__(self,Route,Edgelist,Graph):

        self.EdgeList = Edgelist
        self.Route = Route
        self.EdgeRoute = []
        self.Graph = Graph

        for x in range(len(self.Route[0])):
            if x == len(self.Route[0]) - 1:
                break

            for Ed in range(len(self.EdgeList) - 1):
                    
                if self.EdgeList[Ed].ScattOne() == int(Route[0][x]) and self.EdgeList[Ed].ScattTwo() == int(Route[0][x + 1]):
                    self.EdgeRoute.append(self.EdgeList[Ed])

                if self.EdgeList[Ed].ScattTwo() == int(Route[0][x]) and self.EdgeList[Ed].ScattOne() == int(Route[0][x + 1]):
                    self.EdgeRoute.append(self.EdgeList[Ed])

    def HighlightRoute(self):
        for j in range(len(self.EdgeList)):
            self.EdgeList[j].SetColor('black')
        
        for Edge in range(len(self.EdgeRoute)):
                self.EdgeRoute[Edge].SetColor('red')

        for x in range(len(self.EdgeRoute)):
            self.Graph.DrawLine((self.EdgeRoute[x].GiveYone(),self.EdgeRoute[x].GiveXone()), (self.EdgeRoute[x].GiveYtwo(),self.EdgeRoute[x].GiveXtwo()), color = self.EdgeRoute[x].GiveColor(), width = self.EdgeRoute[x].GetThickness()) 
        print(self.EdgeRoute)

