class EdgePoint:
    def __init__(self,Xone,Yone,Xtwo,Ytwo,ScatOne,ScatTwo):
        self.Xone = int(Xone)
        self.Yone = int(Yone)

        self.Xtwo = int(Xtwo)
        self.Ytwo = int(Ytwo)

        self.Color = 'black'

        self.LineThickness = 3

        self.ScatNumberOne = int(ScatOne)
        self.ScatNumberTwo = int(ScatTwo)
        self.StreetName = "None"

    def SetStreetBane(self,Name):
        if self.StreetName != "None":
            print("AlreadyAssigned")
        self.StreetName = Name

    def ReturnStreetNane(self):
        return self.StreetName

    def GiveXone(self):
        return self.Xone

    def GiveYone(self):
        return self.Yone

    def SetLineThickness(self,SetThick):
        self.LineThickness = SetThick

    def GetThickness(self):
        return self.LineThickness

    def GiveXtwo(self):
        return self.Xtwo

    def GiveYtwo(self):
        return self.Ytwo

    def PrimeSet(self):
        self.Color = 'red'

    def PrimeReset(self):
        self.Color = 'black'

    def GiveColor(self):
        return str(self.Color)

    def SetColor(self,Color):
        self.Color = Color

    def ScattOne(self):
        return self.ScatNumberOne

    def ScattTwo(self):
        return self.ScatNumberTwo