from tkinter.constants import HIDDEN
from ShapeDrawer import ShapeDrawer
from GenerateDataStructure import NodeMapData
from ApplicationEngine.main import RouteingTime
import tkinter as tk
import PySimpleGUI as sg   
from datetime import datetime
import os

WindowX = 1400 #int(input("How wide is your screen? "))
WindowY = 600 #int(input("How 'tall'? is your screen? "))

#Calls an object which extracts data from a csv and generates x/y cordinates
FilteredDataObject = NodeMapData(WindowX,WindowY)
FilteredData = FilteredDataObject.ReturnScatsData()

TimeDropdown = []
print("test")

#Prepars Window Structure
layout = [      
[sg.Graph(canvas_size=(WindowX, WindowY), graph_bottom_left=(0,0), graph_top_right=(WindowX, WindowY), background_color='white', key='graph',enable_events=True)],  
[sg.Button("GetDirections", key="GetDirections")],
[sg.T('AlgoType', size=(8,1), justification='left'),sg.Combo(list(['lstm','gru','cnn']), size=(20,1), enable_events=False, key='AlgoSelection',default_value="lstm"),
sg.T('Date', size=(4,1), justification='left'),sg.Combo(list(['2006-10-29','2006-10-30','2006-10-31']), size=(20,1), enable_events=False, key='DateSelection',default_value="2006-10-29"),
[[sg.T('StartingScatSite', size=(39,1), justification='left'),sg.T('EndingLocation', size=(39,1), justification='left')], sg.Input(key='ScatOne',default_text="4821"),
sg.Input(key='ScatTwo',default_text="0970")],sg.T('DepartureTime: ', size=(12,1), justification='left'),sg.Input(key='Hour', size=(3,1), default_text="12"),
sg.T(':', size=(1,1), justification='left'),sg.Input(key='Minutes', size=(3,1), default_text="00"),sg.Combo(list(['AM','PM']), size=(3,1), enable_events=False, key='AMnPM',
default_value="AM")]]    
window = sg.Window('Graph test', layout, finalize=True)           
graph = window['graph'] 

root = tk.Tk()
exitFlag = False

#Creates an object responcibile for drawing nodes and edges to screen
Map = ShapeDrawer(graph)

window["graph"].bind("<Button-1>", "left")
window["graph"].bind("<Button-3>", "right")

#Generates the data strucutre inside of the shapedrawer
for x in range(len(FilteredData)):
    Map.AddNode(FilteredData[x][0],FilteredData[x][4],FilteredData[x][5],FilteredData[x][3],FilteredData[x][1],FilteredData[x][2])
    #print(FilteredData[x][0])

Map.add_edge('0970', '3685')
Map.add_edge('0970', '2846')
Map.add_edge('2000', '3682')
Map.add_edge('2000', '3685')
Map.add_edge('2000', '4043')
Map.add_edge('2200', '4063')
Map.add_edge('2200', '3126')
Map.add_edge('2820', '3662')
Map.add_edge('2825', '4030')
Map.add_edge('2827', '4051')
Map.add_edge('2846', '0970')
Map.add_edge('2846', '4043')
Map.add_edge('3001', '4262')
Map.add_edge('3001', '3662')
Map.add_edge('3001', '3002')
Map.add_edge('3001', '4821')
Map.add_edge('3002', '3001')
Map.add_edge('3002', '4035')
Map.add_edge('3002', '3662')
Map.add_edge('3002', '4263')
Map.add_edge('3120', '4035')
Map.add_edge('3120', '3122')
Map.add_edge('3120', '4040')
Map.add_edge('3122', '3120')
Map.add_edge('3122', '3127')
Map.add_edge('3122', '3804')
Map.add_edge('3126', '2200')
Map.add_edge('3126', '3682')
Map.add_edge('3126', '3127')
Map.add_edge('3127', '4063')
Map.add_edge('3127', '3122')
Map.add_edge('3127', '3126')
Map.add_edge('3180', '4051')
Map.add_edge('3180', '4057')
Map.add_edge('3662', '3002')
Map.add_edge('3662', '3001')
Map.add_edge('3662', '2820')
Map.add_edge('3662', '4335')
Map.add_edge('3662', '4324')
Map.add_edge('3682', '3126')
Map.add_edge('3682', '3804')
Map.add_edge('3682', '2000')
Map.add_edge('3685', '2000')
Map.add_edge('3685', '0970')
Map.add_edge('3804', '3682')
Map.add_edge('3804', '3812')
Map.add_edge('3804', '4040')
Map.add_edge('3804', '3122')
Map.add_edge('3812', '3804')
Map.add_edge('3812', '4040')
Map.add_edge('4030', '4051')
Map.add_edge('4030', '2825')
Map.add_edge('4030', '4321')
Map.add_edge('4032', '4030')
Map.add_edge('4032', '4321')
Map.add_edge('4032', '4057')
Map.add_edge('4032', '4034')
Map.add_edge('4034', '4063')
Map.add_edge('4034', '4032')
Map.add_edge('4034', '4035')
Map.add_edge('4034', '4324')
Map.add_edge('4035', '3002')
Map.add_edge('4035', '3120')
Map.add_edge('4035', '4034')
Map.add_edge('4040', '4272')
Map.add_edge('4040', '4266')
Map.add_edge('4040', '3120')
Map.add_edge('4040', '3804')
Map.add_edge('4040', '3812')
Map.add_edge('4040', '4043')
Map.add_edge('4043', '2846')
Map.add_edge('4043', '2000')
Map.add_edge('4043', '4273')
Map.add_edge('4043', '4040')
Map.add_edge('4051', '2827')
Map.add_edge('4051', '4030')
Map.add_edge('4051', '3180')
Map.add_edge('4057', '3180')
Map.add_edge('4057', '4032')
Map.add_edge('4057', '4063')
Map.add_edge('4063', '4057')
Map.add_edge('4063', '2200')
Map.add_edge('4063', '3127')
Map.add_edge('4063', '4034')
Map.add_edge('4262', '4263')
Map.add_edge('4262', '3001')
Map.add_edge('4263', '4264')
Map.add_edge('4263', '4262')
Map.add_edge('4263', '3002')
Map.add_edge('4264', '4270')
Map.add_edge('4264', '4266')
Map.add_edge('4264', '4263')
Map.add_edge('4264', '4324')
Map.add_edge('4266', '4264')
Map.add_edge('4266', '4040')
Map.add_edge('4270', '4272')
Map.add_edge('4270', '4264')
Map.add_edge('4270', '4812')
Map.add_edge('4272', '4273')
Map.add_edge('4272', '4040')
Map.add_edge('4272', '4270')
Map.add_edge('4273', '4043')
Map.add_edge('4273', '4272')
Map.add_edge('4321', '4030')
Map.add_edge('4321', '4335')
Map.add_edge('4324', '3662')
Map.add_edge('4324', '4034')
Map.add_edge('4324', '4264')
Map.add_edge('4335', '3662')
Map.add_edge('4335', '4321')
Map.add_edge('4812', '4270')
Map.add_edge('4821', '3001')

#Map.HighLightRoute(Route)
Map.AssociateStreets()
Map.DrawEdges()
Map.DrawNodes()

def clear():
    command = 'clear'
    if os.name in ('nt', 'dos'):  # If Machine is running on Windows, use cls
        command = 'cls'
    os.system(command)

def UpdatePlot():
    graph.erase()
    Map.DrawEdges()
    Map.DrawNodes()

def UpdatePlotWithRoute(RouteNum = int(0)):
    graph.erase()
    Map.DrawEdges()
    Map.DrawNodes()
    Map.DisplayRoute(RouteNum)

def popupmsg(msg):
    popup = tk.Tk()
    popup.wm_title("!")
    label = tk.Label(popup, text=msg, font="Arial")
    label.pack(side="top", fill="x", pady=10)
    B1 = tk.Button(popup, text="Okay", command = popup.destroy)
    B1.pack()
    popup.mainloop()

def on_quit():
    print("No, not happening.")

def RouteWindow():
    global root

    qtyOfRoutes = Map.RouteQTY()
    ButtonList = []   
    
    for widgets in root.winfo_children():
        widgets.destroy()

    frame = tk.Frame(root)
    frame.pack()

    for x in range(qtyOfRoutes):
        ButtonList.append(tk.Button(frame, text="Route"+str(x), fg="red",command=lambda x = x: UpdatePlotWithRoute(x)))

    for Y in range(len(ButtonList)):
        ButtonList[Y].pack(side=tk.LEFT)

    root.mainloop()

def convert24(str1):
    if str1[-2:] == "AM" and str1[:2] == "12":
        return "00" + str1[2:-2]
            
    elif str1[-2:] == "AM":
        return str1[:-2]
        
    elif str1[-2:] == "PM" and str1[:2] == "12":
        return str1[:-2]
          
    else:
        return str(int(str1[:2]) + 12) + str1[2:8]

root.protocol("WM_DELETE_WINDOW", on_quit)

while True:      
    event, values = window.read()      
    mouse = values['graph']

    ScatOne = window['ScatOne']
    ScatTwo = window['ScatTwo']

    if event == sg.WINDOW_CLOSED:
        break

    if event == "graph" +'left':
        ScatOne.update(value = Map.FindClosestNode(mouse,"To"))
        UpdatePlot()

    if event == "graph" +'right':
        ScatTwo.update(value = Map.FindClosestNode(mouse,"From"))
        UpdatePlot()

    if event == "GetDirections":
        Hour = window['Hour'].Get().strip()
        Minutes = window['Minutes'].Get().strip()
        PMnAM = window['AMnPM'].Get().strip()
        ScatOne = window['ScatOne'].Get().strip()
        ScatTwo = window['ScatTwo'].Get().strip()

        Date = window['DateSelection'].Get().strip()

        AlgoType = window['AlgoSelection'].Get().strip()
        ErrorString = ""

        if AlgoType == "":
            ErrorString = ErrorString + "\nYou must enter an algotype."

        if Hour == "" or Minutes == "":
            ErrorString = ErrorString + "\nYou must enter both a start and end time"
        elif Hour.isnumeric() == False or Minutes.isnumeric() == False:
            ErrorString = ErrorString + "\nThe time feild can only accept a numeric entry"
        elif int(Hour) > 12:
            ErrorString = ErrorString + "\nYou must enter a value that is equal or less then 12 for hours"
        elif int(Hour) <= 0:
            ErrorString = ErrorString + "\nYou must enter a valid entry for hours."
        elif int(Minutes) < 0:
            ErrorString = ErrorString + "\nYou must enter a valid entry for minutes."
        elif int(Minutes) > 59:
            ErrorString = ErrorString + "\n The max number of minutes allowed is 59."


        if PMnAM == "":
            ErrorString = ErrorString + "\nYou must select either AM or PM."

        if len(Hour) == 1:
            Hour = '0' + Hour

        if len(Minutes) == 1:
            Minutes = '0' + Minutes

        TimeAssembly = Hour+":"+Minutes+":00 "+PMnAM
        TimeAssembly  = convert24(TimeAssembly).strip()

        dateTime = Date+" "+TimeAssembly
        dateTime = datetime.strptime(dateTime, '%Y-%m-%d %H:%M:%S')

        LowerBoundDate = datetime.strptime("2006-10-29 00:45:00", '%Y-%m-%d %H:%M:%S')
        UpperBoundDate = datetime.strptime("2006-10-31 23:45:00", '%Y-%m-%d %H:%M:%S')

        if (dateTime < LowerBoundDate or dateTime > UpperBoundDate):
            ErrorString = ErrorString + "\n Invalid date"

        if ScatOne != "" or ScatTwo != "":
            if Map.CheckNode(ScatOne) == False:
                ErrorString = ErrorString + "\nYour selected start site doesn't exist."
            elif Map.CheckNode(ScatTwo) == False:
                ErrorString = ErrorString + "\nYour selected destination site doesn't exist."
        else:
            ErrorString = ErrorString + "\nYou must enter a scat site for both the starting and ending location."
        if ErrorString != "":
            popupmsg(ErrorString)
        else:

            AIengine = RouteingTime()
            Route = AIengine.GetRoute(ScatOne,ScatTwo,AlgoType,str(dateTime))
            Map.BuildRoutes(Route)
        
            UpdatePlotWithRoute()
            RouteWindow()

    



 